﻿using ADIFutils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

/*
 * TODO/Bug list
 * 
 * Keep count of # of times state/country is confirmed and display on confirmed list
 * Replace datagrid with listview
 * Fix anchor for filters at the bottom of states and countries panels
 * 
 * 
 * Fix Completed
 * 2018-12-31 Form reize using resizer code
 * 2019-01-01 When reloading ADIF file, bands and modes are duplicated on filter dropdown
 * 2019-01-01 When filters are disabled show NoFilter text on state/country panels
 * 2019-01-01 Remove resizer control, use anchor instead
 * 2019-01-28 Alaska not showing in list of states confirmed/worked
 *            Alaska and Hawaii are listed as their own countries 
 * 2019-04-20 Add README.md
 *            If country is blank or null set country to "??"
 *            Move README.md to solution directory
*/ 

namespace ADIFLogStatP
{

    public partial class frmMain : Form
    {
        private const string NoFilter = "-none-";  // Text for inactive filter
        SortedDictionary<string, WorkedEntityInfo> dictCountries =
            new SortedDictionary<string, WorkedEntityInfo>();
        SortedDictionary<string, WorkedEntityInfo> dictStates =
            new SortedDictionary<string, WorkedEntityInfo>();
        List<QSO> qsos; // All QSOs in the log
        List<LoTWuser> LoTWusers = new List<LoTWuser>();
        bool FiltersEnabled = false;


        public frmMain()
        {
            InitializeComponent();

            lblLoTWerror.Visible = false; //Hide LoTW error message display area
            cboBandFilter.Text = NoFilter;
            cboModeFilter.Text = NoFilter;
            lblStateBand.Text = NoFilter;
            lblStateMode.Text = NoFilter;
            lblCountryBand.Text = NoFilter;
            lblCountryMode.Text = NoFilter;

        }


        private void btnSelectAdif_Click(object sender, EventArgs e)
        {
            ADIFparser parser; //ADIF file parser routine

            //Select an ADIF file and parse the qsos in it
            if (openADIFdialog.ShowDialog() == DialogResult.OK)
            {
                qsos = null;
                txtADIFfileName.Text = openADIFdialog.FileName;
                string all = File.ReadAllText(txtADIFfileName.Text);
                parser = new ADIFparser(all);
                parser.Parse();
                qsos = parser.QSO_List.ToList();
            }

            BuildFilterLists();
            ReshowDictionaries(); //Display the data in each of the QSO dictionaries
        }

        private void btnSelectLoTW_Click(object sender, EventArgs e)
        {
            //Select a LoTW users file and parse the calls in it
            if (openLoTWdialog.ShowDialog() == DialogResult.OK)
            {
                txtLoTWfileName.Text = openLoTWdialog.FileName;
                lblLoTWFileLastUpdated.Text = "LoTW user list last updated " + File.GetLastWriteTime(txtLoTWfileName.Text).ToString("yyyy-MM-dd");

                FileStream fileStream = new FileStream(txtLoTWfileName.Text, FileMode.Open, FileAccess.Read);
                using (StreamReader streamReader = new StreamReader(fileStream))
                {

                    LoTWusers.Clear();
                    string LoTWline;
                    string[] LoTWsplit = new string[3];
                    char[] LoTWsplitDlm = new char[] { ',' };
                    while ((LoTWline = streamReader.ReadLine()) != null)
                    {
                        LoTWsplit = LoTWline.Split(LoTWsplitDlm);
                        LoTWuser newUser = new LoTWuser
                        {
                            LoTWcall = LoTWsplit[0],
                            LoTWlastUpload = LoTWsplit[1]
                        };
                        LoTWusers.Add(newUser);
                    }
                }
            }
        }


        // Go through each QSO and add it to the proper QSO dictionaries
        private void BuildDictionaries()
        {
            if (qsos == null) return;
            // Clear the dictionaries before populating them
            dictCountries.Clear();
            dictStates.Clear();

            foreach (var qso in qsos)
            {
                // If country name is not available use "??" as country name
                if (String.IsNullOrEmpty(qso.country))
                    {
                    qso.country = "??";
                    }

                if (FiltersEnabled)
                {
                    // If the band or mode filter is enabled but does not match 
                    // the QSO then skip the QSO
                    // NOTE THE ! (boolean not) IN THE IF STATEMENTS BELOW
                    if (!((qso.band == cboBandFilter.Text) || (cboBandFilter.Text == NoFilter)))
                    {
                        continue;
                    }
                    if (!((qso.mode == cboModeFilter.Text) || (cboModeFilter.Text == NoFilter)))
                    {
                        continue;
                    }
                }

                UpdateCountryDictionary(qso);

                if ((qso.country == "USA")|| (qso.country=="Alaska")||
                    (qso.country=="Hawaii"))  UpdateStateDictionary(qso);
            }
        }

        /// <summary>
        /// Build a sorted dictionary for the country of each QSO
        /// </summary>
        /// <param name="qso">Information for one contact</param>
        private void UpdateCountryDictionary(QSO qso)
        {
            WorkedEntityInfo entityinfo = new WorkedEntityInfo();

            if (dictCountries.TryGetValue(qso.country, out entityinfo))
            {
                entityinfo.timesWorked++;

                // If the country has been confirmed on LoTW set its
                // confirmed status to true
                if (qso.lotw_qsl_rcvd)
                {
                    entityinfo.confirmed = true;
                }

                dictCountries[qso.country] = entityinfo;
            }
            else
            {
                entityinfo = new WorkedEntityInfo();
                entityinfo.timesWorked = 1;
                entityinfo.confirmed = qso.lotw_qsl_rcvd;
                dictCountries.Add(qso.country, entityinfo);
            }
        }

        /// <summary>
        /// Build a sorted dictionary for the state of each QSO
        /// </summary>
        /// <param name="qso">Information for one contact</param>

        private void UpdateStateDictionary(QSO qso)
        {
            if (qso.state == null) qso.state = "??"; // Unknown state

            WorkedEntityInfo entityinfo = new WorkedEntityInfo();

            if (dictStates.TryGetValue(qso.state, out entityinfo))
            {
                entityinfo.timesWorked++;

                // If the state has been confirmed on LoTW set its
                // confirmed status to true
                if (qso.lotw_qsl_rcvd)
                {
                    entityinfo.confirmed = true;
                }

                dictStates[qso.state] = entityinfo;
            }
            else
            {
                entityinfo = new WorkedEntityInfo
                {
                    timesWorked = 1,
                    confirmed = qso.lotw_qsl_rcvd
                };
                dictStates.Add(qso.state, entityinfo);
            }
        }

        /// <summary>
        /// Build lists of each unique band and mode worked
        /// </summary>
        void BuildFilterLists()
        {
            List<string> bands = new List<string> { };
            List<string> modes = new List<string> { };

            foreach (QSO qso in qsos)
            {
                if (!bands.Contains(qso.band))
                {
                    bands.Add(qso.band);
                }

                if (!modes.Contains(qso.mode))
                {
                    modes.Add(qso.mode);
                }
            }

            // Create filter lists for bands and modes
            cboBandFilter.Text = NoFilter;
            cboBandFilter.Items.Clear();
            cboBandFilter.Items.Add(NoFilter);
            foreach (string band in bands)
            {
                cboBandFilter.Items.Add(band);
            }

            cboModeFilter.Text = NoFilter;
            cboModeFilter.Items.Clear();
            cboModeFilter.Items.Add(NoFilter);
            foreach (string mode in modes)
            {
                cboModeFilter.Items.Add(mode);
            }
        }

        private void DisplayDictionaries()
        {
            if (FiltersEnabled)
            {
                lblStateBand.Text = cboBandFilter.Text;
                lblStateMode.Text = cboModeFilter.Text;
                lblCountryBand.Text = cboBandFilter.Text;
                lblCountryMode.Text = cboModeFilter.Text;
            }
            DisplayStateDictionary();
            DisplayCountryDictionary();
        }

        private void DisplayStateDictionary()
        {
            int statesWorkedCount = 0; // Total states worked
            int statesConfirmedCount = 0; // Total states confirmed
            int statesWUCount = 0; // Total states Worked but Unconfirmed
            txtStatesWorked.Text = "";
            txtStatesConfirmed.Text = "";
            txtStatesWU.Text = "";
            string stateLine;

            foreach (KeyValuePair<string, WorkedEntityInfo> kvp in dictStates)
            {
                statesWorkedCount++;
                stateLine = string.Format($"{kvp.Key}  ({kvp.Value.timesWorked})") + Environment.NewLine;
                txtStatesWorked.Text += stateLine;
                if (kvp.Value.confirmed)
                {
                    statesConfirmedCount++;
                    txtStatesConfirmed.Text += stateLine;
                }
                else
                {
                    statesWUCount++;
                    txtStatesWU.Text += stateLine;
                }
            }

            lblStatesWorked.Text = "States worked (" + statesWorkedCount + ")";
            lblStatesConfirmed.Text = "States confirmed (" + statesConfirmedCount + ")";
            lblStatesWU.Text = "States unconfirmed (" + statesWUCount + ")";


        }

        private void DisplayCountryDictionary()
        {
            int CountriesWorkedCount = 0; // Total Countries worked
            int CountriesConfirmedCount = 0; // Total Countries confirmed
            int CountriesWUCount = 0; // Total Countries worked but unconfirmed
            txtCountriesWorked.Text = "";
            txtCountriesConfirmed.Text = "";
            txtCountriesWU.Text = "";
            string countryLine;

            foreach (KeyValuePair<string, WorkedEntityInfo> kvp in dictCountries)
            {
                CountriesWorkedCount++;
                countryLine = string.Format($"{kvp.Key}  ({kvp.Value.timesWorked})") + Environment.NewLine;
                txtCountriesWorked.Text += countryLine;
                if (kvp.Value.confirmed)
                {
                    CountriesConfirmedCount++;
                    txtCountriesConfirmed.Text += countryLine;
                }
                else
                {
                    CountriesWUCount++;
                    txtCountriesWU.Text += countryLine;
                }
            }

            lblCountriesWorked.Text = "Countries worked (" + CountriesWorkedCount + ")";
            lblCountriesConfirmed.Text = "Countries confirmed (" + CountriesConfirmedCount + ")";
            lblCountriesWU.Text = "Countries unconfirmed (" + CountriesWUCount + ")";


        }

        // Search for a call in the LoTW membership list
        // and return all matching calls and the last date
        // they updated LoTW
        private void btnLoTWsearch_Click(object sender, EventArgs e)
        {
            SearchLotw();

        }

        void SearchLotw()
        {
            dgvLotWusers.Rows.Clear();
            if (string.IsNullOrWhiteSpace(txtLoTWsearchCall.Text))
            {
                lblLoTWerror.Text = "No search specified";
                lblLoTWerror.Visible = true;
                dgvLotWusers.Visible = false;
                return;
            }

            if (LoTWusers.Count == 0)
            {
                lblLoTWerror.Text = "LoTW data not loaded";
                lblLoTWerror.Visible = true;
                dgvLotWusers.Visible = false;

                return;
            }

            lblLoTWerror.Visible = false;
            dgvLotWusers.Visible = true;

            txtLoTWsearchCall.Text = txtLoTWsearchCall.Text.ToUpper(); // Upper case search call
            List<LoTWuser> matchedUsers = LoTWusers.FindAll(x => x.LoTWcall.Contains(value: txtLoTWsearchCall.Text));

            foreach (LoTWuser user in matchedUsers)
            {
                dgvLotWusers.Rows.Add(user.LoTWcall, user.LoTWlastUpload);
            }
        }

        private void btnLoTWclear_Click(object sender, EventArgs e)
        {
            txtLoTWsearchCall.Text = ""; // Blank the search field
            txtLoTWsearchCall.Select(); //Move selection to call entry 
        }

        //Information about each state/country/etc that was worked
        public class WorkedEntityInfo
        {
            public bool confirmed { get; set; } // Has the entity been confirmed on LoTW
            public int timesWorked { get; set; } // Number of times the entity was worked
        }


        private void btnDisableFilters_Click(object sender, EventArgs e)
        {
            btnEnableFilters.Enabled = true;
            btnDisableFilters.Enabled = false;
            cboBandFilter.Text = NoFilter;
            cboModeFilter.Text = NoFilter;
            FiltersEnabled = false;
            ReshowDictionaries();
        }

        private void btnEnableFilters_Click(object sender, EventArgs e)
        {
            btnDisableFilters.Enabled = true;
            btnEnableFilters.Enabled = false;
            FiltersEnabled = true;
            ReshowDictionaries();
        }

        void ReshowDictionaries()
        {
            BuildDictionaries(); //Build QSO dictionaries
            DisplayDictionaries(); //Display the data in each of the QSO dictionaries
        }


        private void cboBandFilter_SelectedValueChanged(object sender, EventArgs e)
        {
            cboBandFilter.Text = cboBandFilter.SelectedItem.ToString();
            ReshowDictionaries();
        }

        private void cboModeFilter_SelectedValueChanged(object sender, EventArgs e)
        {
            cboModeFilter.Text = cboModeFilter.SelectedItem.ToString();
            ReshowDictionaries();
        }

        // If enter is hit while entering the search argument
        // perform the search and suppress the keypress
        private void txtLoTWsearchCall_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                SearchLotw();
                e.SuppressKeyPress = true;
            }
        }
    }

    //Information about each member of LoTW
    public class LoTWuser
    {
        public string LoTWcall; //Call of LoTW user
        public string LoTWlastUpload; //Date of last upload to LoTW
    }

}
