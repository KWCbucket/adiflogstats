﻿namespace ADIFLogStatP
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.SetupPage = new System.Windows.Forms.TabPage();
            this.cboBandFilter = new System.Windows.Forms.ComboBox();
            this.cboModeFilter = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSelectLoTW = new System.Windows.Forms.Button();
            this.txtLoTWfileName = new System.Windows.Forms.TextBox();
            this.btnSelectAdif = new System.Windows.Forms.Button();
            this.txtADIFfileName = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnDisableFilters = new System.Windows.Forms.Button();
            this.btnEnableFilters = new System.Windows.Forms.Button();
            this.StatesPage = new System.Windows.Forms.TabPage();
            this.txtStatesWU = new System.Windows.Forms.TextBox();
            this.lblStatesWU = new System.Windows.Forms.Label();
            this.txtStatesConfirmed = new System.Windows.Forms.TextBox();
            this.txtStatesWorked = new System.Windows.Forms.TextBox();
            this.lblStatesConfirmed = new System.Windows.Forms.Label();
            this.lblStatesWorked = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.lblStateBand = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblStateMode = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.CountriesPage = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblCountryMode = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblCountryBand = new System.Windows.Forms.Label();
            this.txtCountriesWU = new System.Windows.Forms.TextBox();
            this.lblCountriesWU = new System.Windows.Forms.Label();
            this.txtCountriesConfirmed = new System.Windows.Forms.TextBox();
            this.txtCountriesWorked = new System.Windows.Forms.TextBox();
            this.lblCountriesConfirmed = new System.Windows.Forms.Label();
            this.lblCountriesWorked = new System.Windows.Forms.Label();
            this.LoTWPage = new System.Windows.Forms.TabPage();
            this.lblLoTWerror = new System.Windows.Forms.Label();
            this.dgvLotWusers = new System.Windows.Forms.DataGridView();
            this.dgvcLotWcall = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcLoTWlastUpload = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnLoTWclear = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnLoTWsearch = new System.Windows.Forms.Button();
            this.txtLoTWsearchCall = new System.Windows.Forms.TextBox();
            this.lblLoTWFileLastUpdated = new System.Windows.Forms.Label();
            this.openADIFdialog = new System.Windows.Forms.OpenFileDialog();
            this.openLoTWdialog = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1.SuspendLayout();
            this.SetupPage.SuspendLayout();
            this.panel1.SuspendLayout();
            this.StatesPage.SuspendLayout();
            this.panel3.SuspendLayout();
            this.CountriesPage.SuspendLayout();
            this.panel2.SuspendLayout();
            this.LoTWPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLotWusers)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.SetupPage);
            this.tabControl1.Controls.Add(this.StatesPage);
            this.tabControl1.Controls.Add(this.CountriesPage);
            this.tabControl1.Controls.Add(this.LoTWPage);
            this.tabControl1.Location = new System.Drawing.Point(0, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(1035, 702);
            this.tabControl1.TabIndex = 99;
            // 
            // SetupPage
            // 
            this.SetupPage.Controls.Add(this.cboBandFilter);
            this.SetupPage.Controls.Add(this.cboModeFilter);
            this.SetupPage.Controls.Add(this.label4);
            this.SetupPage.Controls.Add(this.label3);
            this.SetupPage.Controls.Add(this.label2);
            this.SetupPage.Controls.Add(this.btnSelectLoTW);
            this.SetupPage.Controls.Add(this.txtLoTWfileName);
            this.SetupPage.Controls.Add(this.btnSelectAdif);
            this.SetupPage.Controls.Add(this.txtADIFfileName);
            this.SetupPage.Controls.Add(this.panel1);
            this.SetupPage.Location = new System.Drawing.Point(4, 29);
            this.SetupPage.Name = "SetupPage";
            this.SetupPage.Padding = new System.Windows.Forms.Padding(3);
            this.SetupPage.Size = new System.Drawing.Size(1027, 669);
            this.SetupPage.TabIndex = 0;
            this.SetupPage.Text = "Setup";
            this.SetupPage.UseVisualStyleBackColor = true;
            // 
            // cboBandFilter
            // 
            this.cboBandFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboBandFilter.FormattingEnabled = true;
            this.cboBandFilter.Location = new System.Drawing.Point(716, 153);
            this.cboBandFilter.Name = "cboBandFilter";
            this.cboBandFilter.Size = new System.Drawing.Size(115, 37);
            this.cboBandFilter.TabIndex = 10;
            this.cboBandFilter.Text = "Bfilter";
            this.cboBandFilter.SelectedValueChanged += new System.EventHandler(this.cboBandFilter_SelectedValueChanged);
            // 
            // cboModeFilter
            // 
            this.cboModeFilter.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboModeFilter.FormattingEnabled = true;
            this.cboModeFilter.Location = new System.Drawing.Point(840, 154);
            this.cboModeFilter.Name = "cboModeFilter";
            this.cboModeFilter.Size = new System.Drawing.Size(115, 37);
            this.cboModeFilter.TabIndex = 9;
            this.cboModeFilter.Text = "Mfilter";
            this.cboModeFilter.SelectedValueChanged += new System.EventHandler(this.cboModeFilter_SelectedValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(840, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 29);
            this.label4.TabIndex = 8;
            this.label4.Text = "Mode";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(716, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 29);
            this.label3.TabIndex = 6;
            this.label3.Text = "Band";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(716, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(233, 82);
            this.label2.TabIndex = 5;
            this.label2.Text = "Filters";
            // 
            // btnSelectLoTW
            // 
            this.btnSelectLoTW.Location = new System.Drawing.Point(8, 120);
            this.btnSelectLoTW.Name = "btnSelectLoTW";
            this.btnSelectLoTW.Size = new System.Drawing.Size(235, 35);
            this.btnSelectLoTW.TabIndex = 4;
            this.btnSelectLoTW.Text = "Click to select LoTW users file";
            this.btnSelectLoTW.UseVisualStyleBackColor = true;
            this.btnSelectLoTW.Click += new System.EventHandler(this.btnSelectLoTW_Click);
            // 
            // txtLoTWfileName
            // 
            this.txtLoTWfileName.Location = new System.Drawing.Point(8, 161);
            this.txtLoTWfileName.Name = "txtLoTWfileName";
            this.txtLoTWfileName.Size = new System.Drawing.Size(676, 26);
            this.txtLoTWfileName.TabIndex = 3;
            // 
            // btnSelectAdif
            // 
            this.btnSelectAdif.Location = new System.Drawing.Point(8, 21);
            this.btnSelectAdif.Name = "btnSelectAdif";
            this.btnSelectAdif.Size = new System.Drawing.Size(235, 35);
            this.btnSelectAdif.TabIndex = 2;
            this.btnSelectAdif.Text = "Click to select ADIF file";
            this.btnSelectAdif.UseVisualStyleBackColor = true;
            this.btnSelectAdif.Click += new System.EventHandler(this.btnSelectAdif_Click);
            // 
            // txtADIFfileName
            // 
            this.txtADIFfileName.Location = new System.Drawing.Point(8, 62);
            this.txtADIFfileName.Name = "txtADIFfileName";
            this.txtADIFfileName.Size = new System.Drawing.Size(676, 26);
            this.txtADIFfileName.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Aquamarine;
            this.panel1.Controls.Add(this.btnDisableFilters);
            this.panel1.Controls.Add(this.btnEnableFilters);
            this.panel1.Location = new System.Drawing.Point(701, 6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(262, 298);
            this.panel1.TabIndex = 11;
            // 
            // btnDisableFilters
            // 
            this.btnDisableFilters.BackColor = System.Drawing.Color.Transparent;
            this.btnDisableFilters.Enabled = false;
            this.btnDisableFilters.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDisableFilters.Location = new System.Drawing.Point(50, 242);
            this.btnDisableFilters.Name = "btnDisableFilters";
            this.btnDisableFilters.Size = new System.Drawing.Size(174, 45);
            this.btnDisableFilters.TabIndex = 0;
            this.btnDisableFilters.Text = "Disable filters";
            this.btnDisableFilters.UseVisualStyleBackColor = false;
            this.btnDisableFilters.Click += new System.EventHandler(this.btnDisableFilters_Click);
            // 
            // btnEnableFilters
            // 
            this.btnEnableFilters.BackColor = System.Drawing.Color.Transparent;
            this.btnEnableFilters.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnableFilters.Location = new System.Drawing.Point(50, 191);
            this.btnEnableFilters.Name = "btnEnableFilters";
            this.btnEnableFilters.Size = new System.Drawing.Size(174, 45);
            this.btnEnableFilters.TabIndex = 0;
            this.btnEnableFilters.Text = "Enable filters";
            this.btnEnableFilters.UseVisualStyleBackColor = false;
            this.btnEnableFilters.Click += new System.EventHandler(this.btnEnableFilters_Click);
            // 
            // StatesPage
            // 
            this.StatesPage.Controls.Add(this.txtStatesWU);
            this.StatesPage.Controls.Add(this.lblStatesWU);
            this.StatesPage.Controls.Add(this.txtStatesConfirmed);
            this.StatesPage.Controls.Add(this.txtStatesWorked);
            this.StatesPage.Controls.Add(this.lblStatesConfirmed);
            this.StatesPage.Controls.Add(this.lblStatesWorked);
            this.StatesPage.Controls.Add(this.panel3);
            this.StatesPage.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StatesPage.Location = new System.Drawing.Point(4, 29);
            this.StatesPage.Name = "StatesPage";
            this.StatesPage.Padding = new System.Windows.Forms.Padding(3);
            this.StatesPage.Size = new System.Drawing.Size(1027, 669);
            this.StatesPage.TabIndex = 1;
            this.StatesPage.Text = "States";
            this.StatesPage.UseVisualStyleBackColor = true;
            // 
            // txtStatesWU
            // 
            this.txtStatesWU.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txtStatesWU.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatesWU.Location = new System.Drawing.Point(660, 53);
            this.txtStatesWU.Multiline = true;
            this.txtStatesWU.Name = "txtStatesWU";
            this.txtStatesWU.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtStatesWU.Size = new System.Drawing.Size(232, 555);
            this.txtStatesWU.TabIndex = 5;
            // 
            // lblStatesWU
            // 
            this.lblStatesWU.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblStatesWU.AutoSize = true;
            this.lblStatesWU.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatesWU.Location = new System.Drawing.Point(660, 18);
            this.lblStatesWU.Name = "lblStatesWU";
            this.lblStatesWU.Size = new System.Drawing.Size(246, 32);
            this.lblStatesWU.TabIndex = 4;
            this.lblStatesWU.Text = "States unconfirmed";
            // 
            // txtStatesConfirmed
            // 
            this.txtStatesConfirmed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txtStatesConfirmed.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatesConfirmed.Location = new System.Drawing.Point(337, 53);
            this.txtStatesConfirmed.Multiline = true;
            this.txtStatesConfirmed.Name = "txtStatesConfirmed";
            this.txtStatesConfirmed.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtStatesConfirmed.Size = new System.Drawing.Size(232, 555);
            this.txtStatesConfirmed.TabIndex = 3;
            // 
            // txtStatesWorked
            // 
            this.txtStatesWorked.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txtStatesWorked.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStatesWorked.Location = new System.Drawing.Point(14, 53);
            this.txtStatesWorked.Multiline = true;
            this.txtStatesWorked.Name = "txtStatesWorked";
            this.txtStatesWorked.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtStatesWorked.Size = new System.Drawing.Size(232, 555);
            this.txtStatesWorked.TabIndex = 2;
            // 
            // lblStatesConfirmed
            // 
            this.lblStatesConfirmed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblStatesConfirmed.AutoSize = true;
            this.lblStatesConfirmed.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatesConfirmed.Location = new System.Drawing.Point(337, 18);
            this.lblStatesConfirmed.Name = "lblStatesConfirmed";
            this.lblStatesConfirmed.Size = new System.Drawing.Size(214, 32);
            this.lblStatesConfirmed.TabIndex = 1;
            this.lblStatesConfirmed.Text = "States confirmed";
            // 
            // lblStatesWorked
            // 
            this.lblStatesWorked.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblStatesWorked.AutoSize = true;
            this.lblStatesWorked.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStatesWorked.Location = new System.Drawing.Point(14, 18);
            this.lblStatesWorked.Name = "lblStatesWorked";
            this.lblStatesWorked.Size = new System.Drawing.Size(181, 32);
            this.lblStatesWorked.TabIndex = 0;
            this.lblStatesWorked.Text = "States worked";
            // 
            // panel3
            // 
            this.panel3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel3.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.lblStateBand);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.lblStateMode);
            this.panel3.Controls.Add(this.label8);
            this.panel3.Location = new System.Drawing.Point(303, 620);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(421, 41);
            this.panel3.TabIndex = 18;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(255, 4);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 27);
            this.label6.TabIndex = 7;
            this.label6.Text = "Mode";
            // 
            // lblStateBand
            // 
            this.lblStateBand.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblStateBand.AutoSize = true;
            this.lblStateBand.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblStateBand.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStateBand.Location = new System.Drawing.Point(156, 4);
            this.lblStateBand.Name = "lblStateBand";
            this.lblStateBand.Size = new System.Drawing.Size(44, 27);
            this.lblStateBand.TabIndex = 8;
            this.lblStateBand.Text = "----";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 4);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 26);
            this.label5.TabIndex = 6;
            this.label5.Text = "Filter:\r\n";
            // 
            // lblStateMode
            // 
            this.lblStateMode.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblStateMode.AutoSize = true;
            this.lblStateMode.BackColor = System.Drawing.SystemColors.ControlDark;
            this.lblStateMode.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStateMode.Location = new System.Drawing.Point(327, 4);
            this.lblStateMode.Name = "lblStateMode";
            this.lblStateMode.Size = new System.Drawing.Size(44, 27);
            this.lblStateMode.TabIndex = 9;
            this.lblStateMode.Text = "----";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.ControlDark;
            this.label8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(94, 4);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 27);
            this.label8.TabIndex = 13;
            this.label8.Text = "Band";
            // 
            // CountriesPage
            // 
            this.CountriesPage.Controls.Add(this.panel2);
            this.CountriesPage.Controls.Add(this.txtCountriesWU);
            this.CountriesPage.Controls.Add(this.lblCountriesWU);
            this.CountriesPage.Controls.Add(this.txtCountriesConfirmed);
            this.CountriesPage.Controls.Add(this.txtCountriesWorked);
            this.CountriesPage.Controls.Add(this.lblCountriesConfirmed);
            this.CountriesPage.Controls.Add(this.lblCountriesWorked);
            this.CountriesPage.Location = new System.Drawing.Point(4, 29);
            this.CountriesPage.Name = "CountriesPage";
            this.CountriesPage.Size = new System.Drawing.Size(1027, 669);
            this.CountriesPage.TabIndex = 2;
            this.CountriesPage.Text = "Countries";
            this.CountriesPage.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.panel2.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.lblCountryMode);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.lblCountryBand);
            this.panel2.Location = new System.Drawing.Point(285, 620);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(421, 41);
            this.panel2.TabIndex = 17;
            // 
            // lblCountryMode
            // 
            this.lblCountryMode.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblCountryMode.AutoSize = true;
            this.lblCountryMode.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountryMode.Location = new System.Drawing.Point(327, 4);
            this.lblCountryMode.Name = "lblCountryMode";
            this.lblCountryMode.Size = new System.Drawing.Size(44, 27);
            this.lblCountryMode.TabIndex = 15;
            this.lblCountryMode.Text = "----";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 4);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(79, 26);
            this.label7.TabIndex = 16;
            this.label7.Text = "Filter:";
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(255, 5);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(68, 27);
            this.label9.TabIndex = 13;
            this.label9.Text = "Mode";
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(94, 4);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 27);
            this.label10.TabIndex = 12;
            this.label10.Text = "Band";
            // 
            // lblCountryBand
            // 
            this.lblCountryBand.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.lblCountryBand.AutoSize = true;
            this.lblCountryBand.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountryBand.Location = new System.Drawing.Point(156, 4);
            this.lblCountryBand.Name = "lblCountryBand";
            this.lblCountryBand.Size = new System.Drawing.Size(44, 27);
            this.lblCountryBand.TabIndex = 14;
            this.lblCountryBand.Text = "----";
            // 
            // txtCountriesWU
            // 
            this.txtCountriesWU.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txtCountriesWU.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountriesWU.Location = new System.Drawing.Point(665, 53);
            this.txtCountriesWU.Multiline = true;
            this.txtCountriesWU.Name = "txtCountriesWU";
            this.txtCountriesWU.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCountriesWU.Size = new System.Drawing.Size(285, 560);
            this.txtCountriesWU.TabIndex = 11;
            this.txtCountriesWU.WordWrap = false;
            // 
            // lblCountriesWU
            // 
            this.lblCountriesWU.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblCountriesWU.AutoSize = true;
            this.lblCountriesWU.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountriesWU.Location = new System.Drawing.Point(665, 18);
            this.lblCountriesWU.Name = "lblCountriesWU";
            this.lblCountriesWU.Size = new System.Drawing.Size(293, 32);
            this.lblCountriesWU.TabIndex = 10;
            this.lblCountriesWU.Text = "Countries unconfirmed";
            // 
            // txtCountriesConfirmed
            // 
            this.txtCountriesConfirmed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txtCountriesConfirmed.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountriesConfirmed.Location = new System.Drawing.Point(337, 53);
            this.txtCountriesConfirmed.Multiline = true;
            this.txtCountriesConfirmed.Name = "txtCountriesConfirmed";
            this.txtCountriesConfirmed.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCountriesConfirmed.Size = new System.Drawing.Size(285, 560);
            this.txtCountriesConfirmed.TabIndex = 9;
            this.txtCountriesConfirmed.WordWrap = false;
            // 
            // txtCountriesWorked
            // 
            this.txtCountriesWorked.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.txtCountriesWorked.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCountriesWorked.Location = new System.Drawing.Point(9, 53);
            this.txtCountriesWorked.Multiline = true;
            this.txtCountriesWorked.Name = "txtCountriesWorked";
            this.txtCountriesWorked.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtCountriesWorked.Size = new System.Drawing.Size(285, 560);
            this.txtCountriesWorked.TabIndex = 8;
            this.txtCountriesWorked.WordWrap = false;
            // 
            // lblCountriesConfirmed
            // 
            this.lblCountriesConfirmed.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblCountriesConfirmed.AutoSize = true;
            this.lblCountriesConfirmed.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountriesConfirmed.Location = new System.Drawing.Point(328, 18);
            this.lblCountriesConfirmed.Name = "lblCountriesConfirmed";
            this.lblCountriesConfirmed.Size = new System.Drawing.Size(261, 32);
            this.lblCountriesConfirmed.TabIndex = 7;
            this.lblCountriesConfirmed.Text = "Countries confirmed";
            // 
            // lblCountriesWorked
            // 
            this.lblCountriesWorked.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblCountriesWorked.AutoSize = true;
            this.lblCountriesWorked.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCountriesWorked.Location = new System.Drawing.Point(5, 18);
            this.lblCountriesWorked.Name = "lblCountriesWorked";
            this.lblCountriesWorked.Size = new System.Drawing.Size(228, 32);
            this.lblCountriesWorked.TabIndex = 6;
            this.lblCountriesWorked.Text = "Countries worked";
            // 
            // LoTWPage
            // 
            this.LoTWPage.Controls.Add(this.lblLoTWerror);
            this.LoTWPage.Controls.Add(this.dgvLotWusers);
            this.LoTWPage.Controls.Add(this.btnLoTWclear);
            this.LoTWPage.Controls.Add(this.label1);
            this.LoTWPage.Controls.Add(this.btnLoTWsearch);
            this.LoTWPage.Controls.Add(this.txtLoTWsearchCall);
            this.LoTWPage.Controls.Add(this.lblLoTWFileLastUpdated);
            this.LoTWPage.Location = new System.Drawing.Point(4, 29);
            this.LoTWPage.Name = "LoTWPage";
            this.LoTWPage.Padding = new System.Windows.Forms.Padding(3);
            this.LoTWPage.Size = new System.Drawing.Size(1027, 669);
            this.LoTWPage.TabIndex = 3;
            this.LoTWPage.Text = "LoTW";
            this.LoTWPage.UseVisualStyleBackColor = true;
            // 
            // lblLoTWerror
            // 
            this.lblLoTWerror.AutoSize = true;
            this.lblLoTWerror.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoTWerror.Location = new System.Drawing.Point(68, 165);
            this.lblLoTWerror.Name = "lblLoTWerror";
            this.lblLoTWerror.Size = new System.Drawing.Size(65, 32);
            this.lblLoTWerror.TabIndex = 7;
            this.lblLoTWerror.Text = "-----";
            // 
            // dgvLotWusers
            // 
            this.dgvLotWusers.AllowUserToAddRows = false;
            this.dgvLotWusers.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvLotWusers.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvLotWusers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvLotWusers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcLotWcall,
            this.dgvcLoTWlastUpload});
            this.dgvLotWusers.Location = new System.Drawing.Point(39, 148);
            this.dgvLotWusers.Name = "dgvLotWusers";
            this.dgvLotWusers.ReadOnly = true;
            this.dgvLotWusers.RowHeadersVisible = false;
            this.dgvLotWusers.RowTemplate.Height = 28;
            this.dgvLotWusers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgvLotWusers.ShowEditingIcon = false;
            this.dgvLotWusers.Size = new System.Drawing.Size(449, 511);
            this.dgvLotWusers.TabIndex = 6;
            // 
            // dgvcLotWcall
            // 
            this.dgvcLotWcall.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvcLotWcall.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvcLotWcall.HeaderText = "Call";
            this.dgvcLotWcall.Name = "dgvcLotWcall";
            this.dgvcLotWcall.ReadOnly = true;
            this.dgvcLotWcall.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvcLotWcall.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // dgvcLoTWlastUpload
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvcLoTWlastUpload.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgvcLoTWlastUpload.HeaderText = "Last upload date";
            this.dgvcLoTWlastUpload.Name = "dgvcLoTWlastUpload";
            this.dgvcLoTWlastUpload.ReadOnly = true;
            this.dgvcLoTWlastUpload.Width = 300;
            // 
            // btnLoTWclear
            // 
            this.btnLoTWclear.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoTWclear.Location = new System.Drawing.Point(475, 76);
            this.btnLoTWclear.Name = "btnLoTWclear";
            this.btnLoTWclear.Size = new System.Drawing.Size(109, 47);
            this.btnLoTWclear.TabIndex = 5;
            this.btnLoTWclear.Text = "Clear";
            this.btnLoTWclear.UseVisualStyleBackColor = true;
            this.btnLoTWclear.Click += new System.EventHandler(this.btnLoTWclear_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 33);
            this.label1.TabIndex = 3;
            this.label1.Text = "Call";
            // 
            // btnLoTWsearch
            // 
            this.btnLoTWsearch.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoTWsearch.Location = new System.Drawing.Point(340, 76);
            this.btnLoTWsearch.Name = "btnLoTWsearch";
            this.btnLoTWsearch.Size = new System.Drawing.Size(109, 47);
            this.btnLoTWsearch.TabIndex = 1;
            this.btnLoTWsearch.Text = "Search";
            this.btnLoTWsearch.UseVisualStyleBackColor = true;
            this.btnLoTWsearch.Click += new System.EventHandler(this.btnLoTWsearch_Click);
            // 
            // txtLoTWsearchCall
            // 
            this.txtLoTWsearchCall.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLoTWsearchCall.Location = new System.Drawing.Point(74, 80);
            this.txtLoTWsearchCall.Name = "txtLoTWsearchCall";
            this.txtLoTWsearchCall.Size = new System.Drawing.Size(230, 40);
            this.txtLoTWsearchCall.TabIndex = 0;
            this.txtLoTWsearchCall.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtLoTWsearchCall_KeyDown);
            // 
            // lblLoTWFileLastUpdated
            // 
            this.lblLoTWFileLastUpdated.AutoSize = true;
            this.lblLoTWFileLastUpdated.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoTWFileLastUpdated.Location = new System.Drawing.Point(33, 28);
            this.lblLoTWFileLastUpdated.Name = "lblLoTWFileLastUpdated";
            this.lblLoTWFileLastUpdated.Size = new System.Drawing.Size(271, 33);
            this.lblLoTWFileLastUpdated.TabIndex = 0;
            this.lblLoTWFileLastUpdated.Text = "LoTW file last updated";
            // 
            // openADIFdialog
            // 
            this.openADIFdialog.FileName = " ";
            this.openADIFdialog.Filter = "ADIF files (*.adi)|*.adi|All files (*.*)|*.*";
            this.openADIFdialog.ReadOnlyChecked = true;
            this.openADIFdialog.Title = "Select ADIF file";
            // 
            // openLoTWdialog
            // 
            this.openLoTWdialog.FileName = " ";
            this.openLoTWdialog.Filter = "CSV Files (*.csv)|*.csv|All Files (*.*|*.*";
            this.openLoTWdialog.ReadOnlyChecked = true;
            this.openLoTWdialog.Title = "Select LoTW .csv file";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 704);
            this.Controls.Add(this.tabControl1);
            this.Name = "frmMain";
            this.Text = "ADIF log statistics by Ken Gunther WB2KWC";
            this.tabControl1.ResumeLayout(false);
            this.SetupPage.ResumeLayout(false);
            this.SetupPage.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.StatesPage.ResumeLayout(false);
            this.StatesPage.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.CountriesPage.ResumeLayout(false);
            this.CountriesPage.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.LoTWPage.ResumeLayout(false);
            this.LoTWPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvLotWusers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage SetupPage;
        private System.Windows.Forms.TabPage StatesPage;
        private System.Windows.Forms.OpenFileDialog openADIFdialog;
        private System.Windows.Forms.Button btnSelectAdif;
        private System.Windows.Forms.TextBox txtADIFfileName;
        private System.Windows.Forms.TextBox txtStatesConfirmed;
        private System.Windows.Forms.TextBox txtStatesWorked;
        private System.Windows.Forms.Label lblStatesConfirmed;
        private System.Windows.Forms.Label lblStatesWorked;
        private System.Windows.Forms.TabPage CountriesPage;
        private System.Windows.Forms.TextBox txtStatesWU;
        private System.Windows.Forms.Label lblStatesWU;
        private System.Windows.Forms.TextBox txtCountriesWU;
        private System.Windows.Forms.TextBox txtCountriesConfirmed;
        private System.Windows.Forms.TextBox txtCountriesWorked;
        private System.Windows.Forms.TabPage LoTWPage;
        private System.Windows.Forms.Button btnSelectLoTW;
        private System.Windows.Forms.TextBox txtLoTWfileName;
        private System.Windows.Forms.OpenFileDialog openLoTWdialog;
        private System.Windows.Forms.Label lblLoTWFileLastUpdated;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnLoTWsearch;
        private System.Windows.Forms.TextBox txtLoTWsearchCall;
        private System.Windows.Forms.Button btnLoTWclear;
        private System.Windows.Forms.DataGridView dgvLotWusers;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcLotWcall;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcLoTWlastUpload;
        private System.Windows.Forms.Label lblLoTWerror;
        private System.Windows.Forms.ComboBox cboModeFilter;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboBandFilter;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnEnableFilters;
        private System.Windows.Forms.Button btnDisableFilters;
        private System.Windows.Forms.Label lblStateMode;
        private System.Windows.Forms.Label lblStateBand;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblCountryMode;
        private System.Windows.Forms.Label lblCountryBand;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblCountriesWU;
        private System.Windows.Forms.Label lblCountriesConfirmed;
        private System.Windows.Forms.Label lblCountriesWorked;
        internal System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label8;
    }
}

